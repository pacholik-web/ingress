# Application Proxy

This repo has created to management from the code instances of [Traefik](https://traefik.io/traefik/). Traefik are the cloud native application proxy. It is use eg. in Kubernetes or Swarm as a Ingress.

## How start

Clone this repo in to Your server. Next make sure docker has a network:

- wordproxy,
- observability.

If not, create using command:

```bash
docker network create <network_name>
```

Because this repo have configuration to [dashboard](https://doc.traefik.io/traefik/operations/dashboard/) was secure by [basic auth](https://doc.traefik.io/traefik/middlewares/http/basicauth/), We need to create password file using [htpasswd](https://linux.die.net/man/1/htpasswd).

### manage base auth users

#### create new passwdfile

To create new passwdfile use command:

```bash
htpasswd -c base-auth-file <username>
```

The flage `-c` create file (or if exists it is rewritten and truncated). Because in docker-compose file was configuration to point `base-auth-file`, above example use this as a file name. **So You just swap `<username>` in to Your user name**.
After You use this command, CLI will ask You to fill password.

#### Manage users in passwdfile

<details>
<summary>add user or change password</summary>

To add next user, use this command line:

```bash
htpasswd base-auth-file <username>
```

</details>

<details>
<summary>delete user</summary>
To delete user from passwdfile use command:

```bash
htpasswd -D base-auth-file <username>
```

</details>

### start docker compose

When You have a `base-auth-file`, You can run command:

```bash
docker compose up -d
```

to start Traefik container and his OpenTelemetry Collector.

If you want verify is all good, You can go to `proxy.your-domain.com` and after auth by basic aut, You can see a dashboard. Quickly method is do request into `proxy.your-domain.com/ping`. If is all ok, You get response status 200.
