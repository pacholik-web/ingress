# Development and test

To help development in local machine, in this folder You fined examples of services, rather based on [nginx](https://hub.docker.com/_/nginx) docker file.

## Red and Blue

In folders named `red`, `blue` and `main` are static files of web pages. Docker compose file in a root folder have configuration to start three nginx services with labels for [Traefik](https://doc.traefik.io/traefik/v3.0/routing/providers/docker/#general) and uses the static files.

## What You can tested ?

If You configure [hosts file](https://www.siteground.com/kb/hosts-file) to contain this line:

```txt
127.0.0.1       localhost
127.0.0.1       blue.localhost
127.0.0.1       red.localhost
127.0.0.1       proxy.localhost
```

you can check:

[ ] is Traefik dashboard work [open](http://proxy.localhost)
[ ] is Traefik dashboard are secure by basic auth
[ ] is Traefik route ([Host](https://doc.traefik.io/traefik/v3.0/routing/routers/#host-and-hostregexp)) work [open](http://localhost)
[ ] is Traefik route [HostRegexp](https://doc.traefik.io/traefik/v3.0/routing/routers/#host-and-hostregexp) work [open](http://blue.localhost)
